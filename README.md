# Vue 3 + Vite

- Para la creación del proyecto ejecutamos el siguiente comando
```bash
npm create vite@latest Pokedex -- --template vue
```

Luego de eso 
```bash
cd Pokedex
```
```bash
npm install
```
- Para ejecutar el proyecto
```bash
npm run dev
```

--- 

