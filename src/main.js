import "vuetify/styles";
import { createApp } from "vue";
import App from "./App.vue";
import router from './router'


import { createVuetify } from "vuetify";

import { aliases, mdi } from "vuetify/lib/iconsets/mdi";

import "@mdi/font/css/materialdesignicons.css"; // Ensure you are using css-loader
import * as components from "vuetify/components";
import * as directives from "vuetify/directives";

import { loadFonts } from "./plugins/webfontloader";

loadFonts();

// axios
import axios from "axios";
import { BASE_URL } from "@/config/api.js";
const axiosConfig = {
  "Content-Type": "application/json;charset=UTF-8",
  "Access-Control-Allow-Origin": "*",
  "Access-Control-Allow-Methods": "*",
};

axios.defaults.baseURL = BASE_URL;
axios.defaults.headers.common = axiosConfig;

const app = createApp(App);
const vuetify = createVuetify({
  components,
  directives,
  icons: {
    defaultSet: "mdi",
    aliases,
    sets: {
      mdi,
    },
  },
});

// Pinia
import { createPinia } from "pinia";

// Persistencia createPinia
import { createPersistedState } from "pinia-plugin-persistedstate";

// reset tiendas
const pinia = createPinia();
pinia.use( createPersistedState({ storage: sessionStorage }) );
// Pinia multiple tab
import { PiniaSharedState } from "pinia-shared-state";
pinia.use(
  PiniaSharedState({
    enable: true,
    initialize: true,
    type: "localstorage",
  })
);



app.use(vuetify);
app.use(pinia)
app.use(router)
app.mount("#app");
