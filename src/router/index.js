import {  createRouter,  createWebHashHistory, } from "vue-router";

const router = createRouter({
  history: createWebHashHistory(import.meta.env.BASE_URL), // me veo forzado a este por las paginas que se abren adicionales. Pero hay ver createWebHistory debe funcioanr
  routes: [
    {
      path: "",
      name: "pokedex",
      component: () => import("@/views/Pokedex.vue"),
    },
    {
      path: "/pokemon/:id",
      name: "pokemon",
      component: () => import("@/components/PokemonDetalle.vue"),
    }
  ],
});

export default router;
