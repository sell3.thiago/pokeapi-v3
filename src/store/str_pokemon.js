import { ref } from 'vue'
import { defineStore } from 'pinia'

export const PokemonStore = defineStore('PokemonStore', () => {
  const oPokemon = ref({})
  return { 
    oPokemon,
  }
},{ persist: true })